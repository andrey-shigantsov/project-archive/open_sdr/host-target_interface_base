/*
 * io.c
 *
 *  Created on: 3 авг. 2018 г.
 *      Author: svetozar
 */

#include "io.h"
#include "checksum.h"

#include <SDR/BASE/Multiplex/common.h>

Bool_t IO_Mux_prepare_for_write(symMultiplex_t * This)
{
  symMultiplex_reset(This);
  if (!sym_multiplex_write_uint16(This, 0, 16)) return false; // size
  if (!sym_multiplex_write_uint16(This, 0, 16)) return false; // crc
  return true;
}

Bool_t IO_Mux_prepare_for_send(symMultiplex_t * This)
{
  UInt8_t * Data = symMultiplex_Data(This);
  UInt16_t Size = symMultiplex_Count(This) - SDR_IO_MUX_HEADER_SIZE;

  int idx = 0;

  MUX_GET_AND_WRITE_BITS(16,Size,0,8,Data[idx++],0);
  MUX_GET_AND_WRITE_BITS(16,Size,8,8,Data[idx++],0);

  uint16_t crc = crc_16(Data+SDR_IO_MUX_HEADER_SIZE, Size);
  MUX_GET_AND_WRITE_BITS(16,crc,0,8,Data[idx++],0);
  MUX_GET_AND_WRITE_BITS(16,crc,8,8,Data[idx++],0);

  return true;
}

Bool_t IO_Mux_prepare_for_read(symMultiplex_t * This, UInt16_t * pduSize, Bool_t * crcOk)
{
  UInt16_t n;
  if (!sym_multiplex_read_uint16(This, &n, 16)) return false;

  UInt16_t crc0;
  if (!sym_multiplex_read_uint16(This, &crc0, 16)) return false;
  UInt8_t * Data = symMultiplex_Data(This) + SDR_IO_MUX_HEADER_SIZE;
  UInt16_t crc = crc_16(Data,n);
  Bool_t ok = crc == crc0;
  if (!ok)
    n = 0;

  if (crcOk)
    *crcOk =ok;
  if (pduSize)
    *pduSize = n;

  return true;
}
