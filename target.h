/*
 * target.h
 *
 *  Created on: 2 февр. 2018 г.
 *      Author: svetozar
 */

#ifndef SDR_HOST_TARGET_INTERFACE_BASE_TARGET_H_
#define SDR_HOST_TARGET_INTERFACE_BASE_TARGET_H_

#include <sdr_basic.h>

#ifdef __cplusplus
extern "C"{
#endif

#define TARGET_TMP_FORMAT_MAXLEN 32767

typedef UInt8_t TARGET_DataId_t;

#define TARGET_ResponseId 0x00

#define TARGET_SamplesDataId 0x04
#define TARGET_iqSamplesDataId 0x05

#define TARGET_ElementCommandId 0x06
#define TARGET_PlotCommandId 0x07

#define TARGET_UserData_StartId 0x10

typedef enum
{
  TARGET_respOK,
  TARGET_respFAIL,

  TARGET_systemStarted,
  TARGET_respSTATE,

  TARGET_Params,
  TARGET_NAME,
  TARGET_controlFORMAT,
  TARGET_statusFORMAT,
  TARGET_plotsFORMAT,

  TARGET_LogMsg
} TARGET_Response_t;

typedef enum
{
  TARGET_ControlElement,
  TARGET_StatusElement
} TARGET_ElementType_t;

typedef enum
{
  TARGET_elemCmd_setValue,
  TARGET_elemCmd_setParams
} TARGET_ElementCommand_t;

typedef enum
{
  TARGET_plotCmd_Clear,
  TARGET_plotCmd_setParam
} TARGET_PlotCommand_t;

typedef enum
{
  TARGET_Started,
  TARGET_Running,
  TARGET_Paused,
  TARGET_Stopped
} TARGET_State_t;

typedef struct
{
  UInt32_t timeout_ms;
} TARGET_Params_t;

#ifdef __cplusplus
}
#endif

#endif /* SDR_HOST_TARGET_INTERFACE_BASE_TARGET_H_ */
